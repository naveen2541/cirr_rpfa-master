//Author : Devika Udupa
//Date : 28-Apr-2016
//This is batch class to insert timesheet records for a month,used in Resource Planning App.

global with sharing class RPFA_Create_Timesheet_Batch implements Database.Batchable<sObject>{

    global final String query;
    global final Date batchStartDate; 
    Map<Id,Id> timesheetsMapping = new Map<Id,Id>(); 
    
    global RPFA_Create_Timesheet_Batch(){
        query = 'SELECT Name,(SELECT AssigneeId FROM Assignments) FROM PermissionSet WHERE Name like \'RPFA_G%\'';
        for (RPFA_Time_Sheet_Mapping__c r : [Select RPFA_UserName__c,RPFA_SupervisorName__c From RPFA_Time_Sheet_Mapping__c]) {
            timesheetsMapping.put(r.RPFA_UserName__c, r.RPFA_SupervisorName__c);
        }        
    }
    
    global RPFA_Create_Timesheet_Batch(Date batchStartDate){
        query = 'SELECT Name,(SELECT AssigneeId FROM Assignments) FROM PermissionSet WHERE Name like \'RPFA_G%\'';
        this.batchStartDate = batchStartDate;
        for (RPFA_Time_Sheet_Mapping__c r : [Select RPFA_UserName__c,RPFA_SupervisorName__c From RPFA_Time_Sheet_Mapping__c]) {
            timesheetsMapping.put(r.RPFA_UserName__c, r.RPFA_SupervisorName__c);
        }         
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<RPFA_Time_Sheet__c> timeSheets = new List<RPFA_Time_Sheet__c>();
        for(sobject s : scope){
            PermissionSet ps = (PermissionSet)s;
            for(PermissionsetAssignment perAssgn : ps.Assignments){                
                //Start & End Date LOgic
                Date startDate;
                Date endDate;
                              
                for(integer n=1;n<7;n++){
                    RPFA_Time_Sheet__c ts = new RPFA_Time_Sheet__c();
                    //week1
                    if(n==1){
                        String dayy ='';
                        Date tday = System.today();
                        if(batchStartDate != null){
                            tday = batchStartDate;
                        }
                        
                        Datetime stDay = datetime.newinstance(tday.toStartOfMonth(),Time.newInstance(0,0,0,0));
                        startDate =  stDay.date();
                        dayy = stday.format('EEEE');
                        Datetime edDate = stDay;
                        while(dayy != 'Friday'){                                        
                            stDay = stDay+1;
                            dayy = stday.format('EEEE');
                            if(dayy == 'Friday'){
                                edDate = stDay;
                                break;
                            }
                        }                       
                        endDate = edDate.date();
                    //week 2
                    } else if(n==2){
                        startDate = endDate+1;
                        endDate = startDate+6;   
                    //week 3               
                    } else if(n==3){
                        startDate = endDate+1;
                        endDate = startDate+6;  
                    //week 4                    
                    } else if(n==4){
                        startDate = endDate+1;
                        Date lastDayOfMonth = Date.newinstance(startDate.year(),startDate.month()+1,1)-1;   
                        if(startDate+6 < lastDayOfMonth){                   
                            endDate = startDate+6;  
                        } 
                    //week 5    
                    }else if(n==5){
                        startDate = endDate+1;
                        Date lastDayOfMonth = Date.newinstance(startDate.year(),startDate.month()+1,1)-1;   
                        if(startDate+6 < lastDayOfMonth){
                            endDate = startDate+6;                              
                        }
                        else{
                            continue;                           
                        }
                                        
                    }else if(n==6){
                        startDate = endDate+1;
                        Date lastDayOfMonth = Date.newinstance(startDate.year(),startDate.month()+1,1)-1;   
                        if(startDate+6 > lastDayOfMonth){
                            endDate = lastDayOfMonth;   
                        }
                    }              
                                  
                    ts.RPFA_Start_Date__c = startDate;
                    ts.RPFA_End_Date__c = endDate;
                    ts.RPFA_Function__c = ps.Name.substring(5);  
                    ts.RPFA_Status__c = 'In Progress';
                    ts.OwnerId = perAssgn.AssigneeId;
                    ts.RPFA_Supervisor__c = timesheetsMapping.get(ts.OwnerId);
              
                    timeSheets.add(ts);
                }
            }
        } 
              
        insert timeSheets;
    }
   
    global void finish(Database.BatchableContext BC){
    
    }
}