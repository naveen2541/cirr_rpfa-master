//Author : Devika Udupa
//Date : 09-May-2016
//This is Schedulable class to schedule RPFA_Create_Timesheet_Batch in Resource Planning App.
//and also locking the last month records.

global class RPFA_Create_Timesheet_Batch_Scheduler implements Schedulable {

   global void execute(SchedulableContext sc) {
      RPFA_Create_Timesheet_Batch ctb = new RPFA_Create_Timesheet_Batch();
      database.executebatch(ctb);     
   }
}