//Author Ashish Tondare
//Date : 06-May-2016
//AppCode : RPFA
//This is Data Factory for the Test class used in Resource Planning App.
@isTest
public class RPFA_DataFactory {
    
    //Get RPFA Admin User
    public static User getRPFAAdminUser(){
        Profile pfl = [select id from profile where name='GSO Basic User_Platform'];
        User u1 = new User(alias = 'alias', email='admin@vrap.com',FirstName='Admin',Department='Cirrus',
        emailencodingkey='UTF-8', lastname='VRAPAdmin', languagelocalekey='en_US',
        localesidkey='en_US', profileid = pfl.Id,  country='United States', CommunityNickname = 'RPFAAdmin',
        timezonesidkey='America/Los_Angeles', username='admin@rpfa.com');
        insert u1;   
        
        // Assign Permission Set
        PermissionSet pSet = [SELECT Id from PermissionSet WHERE Name = 'RPFA_Admin'];
        PermissionSetAssignment assignment = New PermissionSetAssignment();
        assignment.AssigneeId=u1.Id;
        assignment.PermissionSetId=pSet.Id;
        insert assignment;
        return u1;
    }
    
	//Get RPFA Supervisor User
    public static User getRPFASupervisor(){
        Profile pfl = [select id from profile where name='GSO Basic User_Platform'];
        User u1 = new User(alias = 'alias', email='sup@vrap.com',FirstName='Supervisor',Department='Cirrus',
        emailencodingkey='UTF-8', lastname='RPFASup', languagelocalekey='en_US',
        localesidkey='en_US', profileid = pfl.Id,  country='United States', CommunityNickname = 'RPFASup',
        timezonesidkey='America/Los_Angeles', username='sup@rpfa.com');
        insert u1;   
        
        // Assign Permission Set
        PermissionSet pSet = [SELECT Id from PermissionSet WHERE Name = 'RPFA_GSC'];
        PermissionSetAssignment assignment = New PermissionSetAssignment();
        assignment.AssigneeId=u1.Id;
        assignment.PermissionSetId=pSet.Id;
        insert assignment;
        return u1;
    }
    
    //Get RPFA Employee
    public static User getRPFAEmployee(){
        Profile pfl = [select id from profile where name='GSO Basic User_Platform'];
        User u1 = new User(alias = 'alias', email='emp@vrap.com',FirstName='Employee',Department='Cirrus',
        emailencodingkey='UTF-8', lastname='RPFAEmp', languagelocalekey='en_US',
        localesidkey='en_US', profileid = pfl.Id,  country='United States', CommunityNickname = 'RPFAEmp',
        timezonesidkey='America/Los_Angeles', username='emp@rpfa.com');
        insert u1;   
        
        // Assign Permission Set
        PermissionSet pSet = [SELECT Id from PermissionSet WHERE Name = 'RPFA_GSC'];
        PermissionSetAssignment assignment = New PermissionSetAssignment();
        assignment.AssigneeId=u1.Id;
        assignment.PermissionSetId=pSet.Id;
        insert assignment;
        return u1;
    }
    
    //Get Time Sheet Data
    public static RPFA_Time_Sheet__c getTimeSheetData(Id uId){
        RPFA_Time_Sheet__c ts = new RPFA_Time_Sheet__c();
        ts.RPFA_Start_Date__c = System.today();
        ts.RPFA_End_Date__c = System.today()+3;
        ts.RPFA_Status__c = 'In Progress';
        insert ts;
        return ts;
    }
    
    //Get Time Sheet Data
    public static RPFA_Time_Sheet_Entry__c  getTimeSheetEntryData(Id uId,Id tsId){
        RPFA_Time_Sheet_Entry__c  tse = new RPFA_Time_Sheet_Entry__c ();
        tse.RPFA_Date__c = system.today()+1;
        tse.RPFA_Biomedicines__c = 2;
        tse.RPFA_Diabetes__c = 3;
        tse.RPFA_Oncology__c = 2;
        tse.RPFA_Non_Molecule__c = 3;
        tse.RPFA_Research__c = 2;
        tse.RPFA_Multi_Molecule__c = 2;
        tse.RPFA_Time_Sheet_Week__c = tsId;
        insert tse;
        return tse;
    }
  
}