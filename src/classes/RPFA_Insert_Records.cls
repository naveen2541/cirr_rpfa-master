public with sharing class RPFA_Insert_Records {
    public RPFA_Time_Sheet__c TS {get;set;}
    public Boolean isSupervisor {get;set;}
    
    public class custException extends Exception{}

    public RPFA_Insert_Records(ApexPages.StandardController controller){
        TS = (RPFA_Time_Sheet__c)controller.getRecord();
    }
    public void insertTimesheet(){
        try{
            List<PermissionSet> ps = [SELECT Name,(SELECT AssigneeId FROM Assignments where AssigneeId =: TS.OwnerId) FROM PermissionSet WHERE Name like 'RPFA_G%']; 
            List<RPFA_Time_Sheet_Mapping__c> tsm = [Select RPFA_UserName__c,RPFA_SupervisorName__c From RPFA_Time_Sheet_Mapping__c where RPFA_UserName__c =:TS.OwnerId limit 1];  
            List<RPFA_Time_Sheet__c> timeSheets = new List<RPFA_Time_Sheet__c>();                

            for(PermissionSet perSet : ps){
                system.debug('perSet.Assignments'+perSet.Assignments);
                if(perSet.Assignments.size()>0){                              
                    for(PermissionsetAssignment perAssgn : perSet.Assignments){
                        //Start & End Date Logic
                        Date startDate;
                        Date endDate;
                        
                        for(integer n=1;n<7;n++){
                            RPFA_Time_Sheet__c tsheet = new RPFA_Time_Sheet__c();
                            //week1
                            if(n==1){
                                String dayy ='';
                                Date tday = System.today();               
                                
                                Datetime stDay = datetime.newinstance(tday.toStartOfMonth(),Time.newInstance(0,0,0,0));
                                startDate =  stDay.date();
                                dayy = stday.format('EEEE');
                                Datetime edDate = stDay;
                                while(dayy != 'Friday'){                                        
                                    stDay = stDay+1;
                                    dayy = stday.format('EEEE');
                                    if(dayy == 'Friday'){
                                        edDate = stDay;
                                        break;
                                    }
                                }                       
                                endDate = edDate.date();
                                //week 2
                            } else if(n==2){
                                startDate = endDate+1;
                                endDate = startDate+6;   
                                //week 3               
                            } else if(n==3){
                                startDate = endDate+1;
                                endDate = startDate+6;  
                                //week 4                    
                            } else if(n==4){
                                startDate = endDate+1;
                                Date lastDayOfMonth = Date.newinstance(startDate.year(),startDate.month()+1,1)-1;   
                                if(startDate+6 < lastDayOfMonth){                   
                                    endDate = startDate+6;  
                                } 
                                //week 5    
                            }else if(n==5){
                                startDate = endDate+1;
                                Date lastDayOfMonth = Date.newinstance(startDate.year(),startDate.month()+1,1)-1;   
                                if(startDate+6 < lastDayOfMonth){
                                    endDate = startDate+6;                              
                                }
                                else{
                                    continue;                           
                                }
                                
                            }else if(n==6){
                                startDate = endDate+1;
                                Date lastDayOfMonth = Date.newinstance(startDate.year(),startDate.month()+1,1)-1;   
                                if(startDate+6 > lastDayOfMonth){
                                    endDate = lastDayOfMonth;   
                                }
                            }              
                            
                            tsheet.RPFA_Start_Date__c = startDate;
                            tsheet.RPFA_End_Date__c = endDate;
                            tsheet.RPFA_Function__c = perSet.Name.substring(5);    
                            tsheet.RPFA_Status__c = 'In Progress';
                            tsheet.OwnerId = TS.OwnerId;
                            //bypass RPFA_SupervisorName__c field for Supervisors
                            if(!isSupervisor && tsm.size()>0){
                                tsheet.RPFA_Supervisor__c = tsm[0].RPFA_SupervisorName__c;
                            } else if(!isSupervisor && tsm.size() == 0){
                               throw new custException('Please create User-Supervisor Mapping before creating timesheet!');                              
                            }
                            timeSheets.add(tsheet);
                        }
                    }
                }
            }
                 
            system.debug('timeSheets'+timeSheets);
            if(timeSheets.size() >0){
                Database.SaveResult[] srList = Database.insert(timeSheets);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info,'Timesheet Record created!'));
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'No Timesheet Record Created! Please check Instructions!'));
            }
        }//end of try
        catch(custException e){
            ApexPages.addMessages(e) ;            
        } 
        catch(Exception e){
            ApexPages.addMessages(e) ; 
        }       
    }   
}