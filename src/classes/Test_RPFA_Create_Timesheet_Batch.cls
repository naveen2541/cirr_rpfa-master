//Author: Devika Udupa
//Date : 06-May-2016
//This is Test class for RPFA_Create_Timesheet_Batch used in Resource Planning App.

@isTest(seeAllData=false)
private class Test_RPFA_Create_Timesheet_Batch {

    public static user rpfaAdmin {get;set;}
    public static user rpfaSup {get;set;}
    public static user rpfaEmp {get;set;}
    
   @isTest
   static void setupTestData() {
       User thisUser = [SELECT Id FROM User WHERE Id = : UserInfo.getUserId()];
       System.runAs(thisUser) {
           rpfaAdmin = RPFA_DataFactory.getRPFAAdminUser();
           rpfaSup = RPFA_DataFactory.getRPFASupervisor();
           rpfaEmp = RPFA_DataFactory.getRPFAEmployee();

           RPFA_Time_Sheet_Mapping__c  tsm = new RPFA_Time_Sheet_Mapping__c ();
            tsm.RPFA_UserName__c = rpfaEmp.Id;
            tsm.RPFA_SupervisorName__c = rpfaSup.Id;
            insert tsm;
            
            System.assert(true);
       }
    }
    
    @isTest
    static void insertTimesheetRecords(){
        setupTestData();
                 
        Test.startTest();
            RPFA_Create_Timesheet_Batch ctb = new RPFA_Create_Timesheet_Batch();
            Database.executeBatch(ctb);                         
        Test.stopTest();
        
        System.runAs(rpfaEmp) {
            List<RPFA_Time_Sheet__c> timeSheets = [Select Id from RPFA_Time_Sheet__c limit 2];
            System.assert(timeSheets.size()>0);
        }          
        
    }
    
    @isTest
    static void insertTimesheetRecordsWithDate(){
        setupTestData();
        
        Test.startTest();
            RPFA_Create_Timesheet_Batch ctb = new RPFA_Create_Timesheet_Batch(system.today());
            Database.executeBatch(ctb);          
        Test.stopTest();
        
        System.runAs(rpfaEmp) {
            List<RPFA_Time_Sheet__c> timeSheets = [Select Id from RPFA_Time_Sheet__c limit 2];
            System.assert(timeSheets.size()>0);
        }  
    }   
    
    //Test method for schedulable class RPFA_Create_Timesheet_Batch_Scheduler
    @isTest
    static void RPFA_Create_Timesheet_Batch_Scheduler_Test(){       
        String sch = '0 0 1 1 * ?';
        
        Test.startTest();
            RPFA_Create_Timesheet_Batch_Scheduler ctbs = new RPFA_Create_Timesheet_Batch_Scheduler();
            String jobId = system.schedule('Test Schedule',sch,ctbs); 
            CronTrigger ct = [SELECT Id, CronExpression FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(sch,ct.CronExpression);         
        Test.stopTest();                 
    } 
    
    
}