//Author: Devika Udupa
//Date : 04-July-2016
//This is Test class for RPFA_Insert_Records controller used in Resource Planning App.

@isTest(seeAllData=false)
private class Test_RPFA_Insert_Records {

    public static user rpfaAdmin {get;set;}
    public static user rpfaSup {get;set;}
    public static user rpfaEmp {get;set;}
    
   @isTest
   static void setupTestData() {
       User thisUser = [SELECT Id FROM User WHERE Id = : UserInfo.getUserId()];
       System.runAs(thisUser) {
           rpfaAdmin = RPFA_DataFactory.getRPFAAdminUser();
           rpfaSup = RPFA_DataFactory.getRPFASupervisor();
           rpfaEmp = RPFA_DataFactory.getRPFAEmployee();
    
           RPFA_Time_Sheet_Mapping__c  tsm = new RPFA_Time_Sheet_Mapping__c ();
           tsm.RPFA_UserName__c = rpfaEmp.Id;
           tsm.RPFA_SupervisorName__c = rpfaSup.Id;
           insert tsm;
           System.assert(true);
       }
    }
    
    @isTest
    static void insertTimesheetRecords(){
        setupTestData();
                 
        Test.startTest();
            RPFA_Time_Sheet__c testTimeSheet = new RPFA_Time_Sheet__c(ownerid=rpfaEmp.Id);            
            ApexPages.StandardController sc = new ApexPages.StandardController(testTimeSheet);
            RPFA_Insert_Records inserRecs = new RPFA_Insert_Records(sc);
            
            PageReference pageRef = Page.RPFA_Insert_Records;          
            Test.setCurrentPage(pageRef);
            
            inserRecs.isSupervisor = false ;           
            inserRecs.insertTimesheet();                         
        Test.stopTest();
        
        System.runAs(rpfaEmp) {
            List<RPFA_Time_Sheet__c> timeSheets = [Select Id from RPFA_Time_Sheet__c limit 2];
            System.assert(timeSheets.size()>0);
        }                
    }
    
    @isTest
    static void insertTimesheetRecordsWithNoMapping(){
        setupTestData();
                 
        Test.startTest();
            RPFA_Time_Sheet__c testTimeSheet = new RPFA_Time_Sheet__c(ownerid=rpfaSup.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(testTimeSheet);
            RPFA_Insert_Records inserRecs = new RPFA_Insert_Records(sc);
            
            PageReference pageRef = Page.RPFA_Insert_Records;           
            Test.setCurrentPage(pageRef);

            inserRecs.isSupervisor = false ;           
            inserRecs.insertTimesheet();                         
        Test.stopTest();
        
        System.runAs(rpfaEmp) {
            List<RPFA_Time_Sheet__c> timeSheets = [Select Id from RPFA_Time_Sheet__c limit 2];
            System.assertEquals(timeSheets.size(),0);
        }          
        
    }
    
    @isTest
    static void insertTimesheetRecordsSupervisor(){
        setupTestData();
                 
        Test.startTest();
            RPFA_Time_Sheet__c testTimeSheet = new RPFA_Time_Sheet__c(ownerid=rpfaSup.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(testTimeSheet);
            RPFA_Insert_Records inserRecs = new RPFA_Insert_Records(sc);
            
            PageReference pageRef = Page.RPFA_Insert_Records;
            Test.setCurrentPage(pageRef);
            
            inserRecs.isSupervisor = true ;           
            inserRecs.insertTimesheet();                         
        Test.stopTest();
        
        System.runAs(rpfaSup) {
            List<RPFA_Time_Sheet__c> timeSheets = [Select Id from RPFA_Time_Sheet__c limit 2];
            System.assert(timeSheets.size()>0);
        }          
        
    }  
    
    @isTest
    static void insertTimesheetRecordsException(){
        setupTestData();
                 
        Test.startTest();
            RPFA_Time_Sheet__c testTimeSheet = new RPFA_Time_Sheet__c();
            ApexPages.StandardController sc = new ApexPages.StandardController(testTimeSheet);
            RPFA_Insert_Records inserRecs = new RPFA_Insert_Records(sc);
            
            PageReference pageRef = Page.RPFA_Insert_Records;           
            Test.setCurrentPage(pageRef);
                      
            inserRecs.isSupervisor = false ;           
            inserRecs.insertTimesheet();                         
        Test.stopTest();
        
        System.runAs(rpfaEmp) {
            List<RPFA_Time_Sheet__c> timeSheets = [Select Id from RPFA_Time_Sheet__c limit 2];
            System.assertEquals(timeSheets.size(),0);
        }          
        
    }            
}