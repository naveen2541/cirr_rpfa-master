@isTest(seeAllData=false)
private class Test_RPFA_Time_Sheet_Sharing {

    public static user rpfaAdmin {get;set;}
    public static user rpfaSup {get;set;}
    public static user rpfaEmp {get;set;}
    public static RPFA_Time_Sheet__c TS{get;set;}
    public static RPFA_Time_Sheet_Entry__c TSE {get;set;}
    
	@isTest
    static void setupTestData() {
        User thisUser = [SELECT Id FROM User WHERE Id = : UserInfo.getUserId()];
        System.runAs(thisUser) {
            rpfaAdmin = RPFA_DataFactory.getRPFAAdminUser();
            rpfaSup = RPFA_DataFactory.getRPFASupervisor();
            rpfaEmp = RPFA_DataFactory.getRPFAEmployee();
            TS = RPFA_DataFactory.getTimeSheetData(rpfaEmp.ID);
            TSE = RPFA_DataFactory.getTimeSheetEntryData(rpfaEmp.Id, TS.Id);
            System.assert(true);
        }
     }
    @isTest
    static void TimeSheetSharing(){
        setupTestData();
        Test.startTest();
       
        TS.RPFA_Supervisor__c = rpfaSup.Id;
        Update TS;
        Test.stopTest();
        System.assert(true);
    }
    
    @isTest
    // manual update of supervisor.
    static void TimeSheetSharing1(){
        setupTestData();
        RPFA_Time_Sheet__c timeSheet = new RPFA_Time_Sheet__c();
        timeSheet.RPFA_Start_Date__c = System.today();
        timeSheet.RPFA_End_Date__c = System.today()+3;
        timeSheet.RPFA_Status__c = 'In Progress';
        timeSheet.RPFA_Supervisor__c = rpfaSup.Id;
        insert timeSheet;
        
        Test.startTest();
        timeSheet.RPFA_Supervisor__c = rpfaAdmin.Id;
        Update timeSheet;
        Test.stopTest();
        System.assert(true);
    }
    
    @isTest
    // manual change of owner
    static void TimeSheetSharing2(){
        setupTestData();
        RPFA_Time_Sheet__c timeSheet1 = new RPFA_Time_Sheet__c();
        System.runAs(rpfaAdmin) {
            timeSheet1.RPFA_Start_Date__c = System.today();
            timeSheet1.RPFA_End_Date__c = System.today()+3;
            timeSheet1.RPFA_Status__c = 'In Progress';
            timeSheet1.OwnerId = rpfaAdmin.Id;
            insert timeSheet1;
        }
        
        Test.startTest();
        timeSheet1.OwnerId = rpfaEmp.Id;
        Update timeSheet1;

        Test.stopTest();
		timeSheet1.RPFA_Supervisor__c = rpfaSup.Id;
        Update timeSheet1;
        
        System.assert(true);
    }
  
}