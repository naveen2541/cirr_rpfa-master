//Author : Ashish Tondare
//Created Date: 03-May-2016
//This trigger was created as part of LCCI Resource Planning Application. 
//This trigger will insert records in Time Sheet share object unpon creation of new Time Sheet by Batch
//enabling Supervisors & Director to view the Data.

trigger RPFA_Time_Sheets_Sharing on RPFA_Time_Sheet__c (after insert,after update) {

    List<RPFA_Time_Sheet__share> tsShare = new List<RPFA_Time_Sheet__share>();
    List<RPFA_Time_Sheet_Entry__c > tseAll = new List<RPFA_Time_Sheet_Entry__c >();
    List<RPFA_Time_Sheet__share> deleteShareData = new List<RPFA_Time_Sheet__share>();
    
	User FinanceDir = [Select ID from User where Email =: LABEL.RPFA_Finance_Director  LIMIT 1];
    Date startDate;
	Date endDate ;
	Integer numberofTse;
    
    //Time Sheet Entries for Each Employee
    If(Trigger.isInsert){
      List<ID> ids= new List<ID>();        
        for(RPFA_Time_Sheet__c ts: Trigger.New){
            startDate = ts.RPFA_Start_Date__c;
            endDate = ts.RPFA_End_Date__c;
            numberofTse = startDate.daysBetween(endDate)+1;
            for(Integer i=0; i < numberofTse; i++ ){
                RPFA_Time_Sheet_Entry__c tse = new RPFA_Time_Sheet_Entry__c();
                tse.RPFA_Time_Sheet_Week__c = ts.Id;
                tse.RPFA_Date__c = startDate.addDays(i);
                tseAll.add(tse);
            }
            //Share All Records with Director 
            
            RPFA_Time_Sheet__share tssFD = new RPFA_Time_Sheet__share();
            if(FinanceDir != null){
                tssFD.ParentId  = ts.Id;
                tssFD.AccessLevel = 'Edit';
                tssFD.RowCause = 'Manual';
                tssFD.UserOrGroupId = FinanceDir.Id;
                tsShare.add(tssFD);
            }
            if(ts.RPFA_Supervisor__c!= null){
                RPFA_Time_Sheet__share tss = new RPFA_Time_Sheet__share();
                tss.ParentId  = ts.Id;
                tss.AccessLevel = 'Edit';
                tss.RowCause = 'Manual';
                tss.UserOrGroupId = ts.RPFA_Supervisor__c;
                tsShare.add(tss);
            }
        }
        
    }
    //Sharing of records with Supervisors 
    if(Trigger.IsUpdate){
        List<RPFA_Time_Sheet__share> oldShareData = [SELECT Id,ParentId,UserOrGroupId 
                                                     	FROM RPFA_Time_Sheet__Share Where ParentId IN: Trigger.new];
        for (RPFA_Time_Sheet__c tsUpdate : Trigger.new) {
            RPFA_Time_Sheet__c oldts = Trigger.oldMap.get(tsUpdate.Id);
            // if supervisor is changed, delete the share record with old supervisor and create a new share record.
            if (oldts.RPFA_Supervisor__c != tsUpdate.RPFA_Supervisor__c) {
                for(RPFA_Time_Sheet__share rpshare : oldShareData){
                    if(rpshare.UserOrGroupId == oldts.RPFA_Supervisor__c){
                        //Prepare Data to Delete share records 
                        deleteShareData.add(rpshare);
                        //Prepare Data to insert share records
                        RPFA_Time_Sheet__share newtss = new RPFA_Time_Sheet__share();
                        newtss.ParentId  = tsUpdate.Id;
                        newtss.AccessLevel = 'Edit';
                        newtss.RowCause = 'Manual';
                        newtss.UserOrGroupId = tsUpdate.RPFA_Supervisor__c;
                        tsShare.add(newtss);
                    }
                    else if(tsUpdate.RPFA_Supervisor__c!= null){
                        RPFA_Time_Sheet__share newtss = new RPFA_Time_Sheet__share();
                        newtss.ParentId  = tsUpdate.Id;
                        newtss.AccessLevel = 'Edit';
                        newtss.RowCause = 'Manual';
                        newtss.UserOrGroupId = tsUpdate.RPFA_Supervisor__c;
                        tsShare.add(newtss);
                    }
                }
            }
            // if the owner of the record is changed, then create new sharing record with supervisor and finance Director.
            else if (oldts.OwnerId != tsUpdate.OwnerId){
                RPFA_Time_Sheet__share tssFD = new RPFA_Time_Sheet__share();
                if(FinanceDir != null){
                    tssFD.ParentId  = tsUpdate.Id;
                    tssFD.AccessLevel = 'Edit';
                    tssFD.RowCause = 'Manual';
                    tssFD.UserOrGroupId = FinanceDir.Id;
                    tsShare.add(tssFD);
                }
                if(tsUpdate.RPFA_Supervisor__c!= null){
                    RPFA_Time_Sheet__share tss = new RPFA_Time_Sheet__share();
                    tss.ParentId  = tsUpdate.Id;
                    tss.AccessLevel = 'Edit';
                    tss.RowCause = 'Manual';
                    tss.UserOrGroupId = tsUpdate.RPFA_Supervisor__c;
                    tsShare.add(tss);
                }
            }
        }    
    }
    
    //Delete Share Records
    if(deleteShareData.size() > 0){
        try{
            Delete deleteShareData;
        }Catch(Exception e){}
    }
    
    //Insert Share Records
    if(tsShare.size() > 0){
        try{
            insert tsShare;
        }Catch(Exception e){}
    }
    //Insert Time Sheet Entries Records
    if(tseAll.size() > 0){
        try{
            insert tseAll;
        }Catch(Exception e){}
    }
}