<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Time_Sheet_Reminder</fullName>
        <description>Time Sheet Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RPFA_Templates/RPFA_Timesheet_Reminder</template>
    </alerts>
    <fieldUpdates>
        <fullName>RPFA_Update_Function</fullName>
        <description>Update function GSMDD to SMDD</description>
        <field>RPFA_Function__c</field>
        <literalValue>SMDD</literalValue>
        <name>RPFA_Update_Function</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>RPFA_Timesheet_Reminder</fullName>
        <active>true</active>
        <formula>AND(RPFA_End_Date__c  &gt;=   TODAY(),  RPFA_Start_Date__c  &lt;=  TODAY() )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Time_Sheet_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>RPFA_Time_Sheet__c.RPFA_End_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>RPFA_Update_Function</fullName>
        <actions>
            <name>RPFA_Update_Function</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RPFA_Time_Sheet__c.RPFA_Function__c</field>
            <operation>equals</operation>
            <value>GSMDD</value>
        </criteriaItems>
        <description>Workflow to update Function value GSMDD to SMDD</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
